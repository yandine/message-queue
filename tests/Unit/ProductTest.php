<?php

namespace Tests\Unit;

use Tests\TestCase;
use Faker;
use Mockery;
use Mockery\MockInterface;
use InvalidArgumentException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Bus;

use App\Repositories\Product\ProductRepository;
use App\Repositories\Product\ProductInterface;

use App\Jobs\ProcessProduct;
use App\Models\Product;

class ProductTest extends TestCase
{	

	use RefreshDatabase;


	 /**
     * Test if adding 100 products 
     *
     * @return void
     */

	public function test_creating_100_products(){

    	$productRepo = new ProductRepository(new Product());

    	for($i=1; $i<=100; $i++){
    		$productFakeData = $this->createFakeProduct($i);

    		$created = $productRepo->create([
	    		'id' => $productFakeData->id,
	    		'name' => $productFakeData->name,
	    		'description' => $productFakeData->description
	    	]);
    	}

    	$this->assertCount(100, $productRepo->getAll()->toArray());
    }


    /**
     * Test Adding single product
     *
     * @return void
     */

    public function test_adding_of_a_single_product()
    {	

    	$productFakeData = $this->createFakeProduct(101);

    	$productRepo = new ProductRepository(new Product());

    	$productRepo->create([
    		'id' => $productFakeData->id,
    		'name' => $productFakeData->name,
    		'description' => $productFakeData->description
    	]);

    	$this->assertEquals($productRepo->getLatest()->id, $productFakeData->id);

    }



	/**
     * Test if adding duplicate ID will fail
     *
     * @return void
     */
    public function test_duplicate_id_should_fail(){
    	$this->withExceptionHandling();

    	$productFakeData = $this->createFakeProduct(121);

    	$productRepo = new ProductRepository(new Product());

    	//First Attempt
    	$productRepo->create([
    		'id' => $productFakeData->id,
    		'name' => $productFakeData->name,
    		'description' => $productFakeData->description
    	]);

	    $mock = Mockery::mock(ProductRepository::class, function(MockInterface $mock){
            $mock->shouldReceive('find')->once()->andReturn((Object)['id' => 121]);
        });

        //Second Attempt
        $mockSearch = $mock->find(121);

    	$productRepo->create([
    		'id' => $mockSearch->id,
    		'name' => 'Custom Name',
    		'description' => 'Custom description'
    	]);

    	$this->assertCount(1, $productRepo->getAll());
    }


    /**
     * TestING Jobs
     *
     * @return void
     */
    public function test_product_dispatched_job()
    {	
    	Queue::fake();
		
    	
    	$counter = 1;
		for($a=1; $a<=10; $a++){

			$data = [];

	    	for($i=1; $i<=5; $i++){
	    		$productFakeData = $this->createFakeProduct($counter);

	    		$data[] = (Object)[
		    		'id' => $productFakeData->id,
		    		'name' => $productFakeData->name,
		    		'description' => $productFakeData->description
		    	];

		    	$counter++;
	    	}

	    	ProcessProduct::dispatch($data);

	    	Queue::assertPushed(ProcessProduct::class);
    	}
    }




    public function test_check_if_home_page_is_accessible(){ 
    	$response = $this->get('/');
        $response->assertStatus(200);
    }


    private function createFakeProduct($id=1)
    {
        $faker = Faker\Factory::create();
        $product = new Product();
        $product->id = $id;
        $product->name = $faker->name;
        $product->description = $faker->name;

        return $product;
    }
}
