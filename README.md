## Instructions to build and run the site

- Clone the files using:
``` git clone git@bitbucket.org:yandine/message-queue.git ```
- Install dependencies 
``` npm install ```
- Create a database and update .env file, you can copy .env.example as reference
- Start Migrating tables
```php artisan migrate:fresh --seed```
- Compiling assets
```npm run dev```
- Run the Application
```php artisan serve```
- Run Tests
```php artisan test```
- Open another cmd instance and enable Queuing in the background
```php artisan queue:work```




