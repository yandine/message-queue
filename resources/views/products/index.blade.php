<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="{{asset('css/app.css')}}">
	</head>
	<body>
		<div class="form">
			Source: https://jsonkeeper.com/b/EB4F <br />
			<a class="a-link-q" href="?start">Click to Start Queuing</a>
		</div>

		<script src="{{asset('js/app.js')}}"></script>
	</body>
</html>
