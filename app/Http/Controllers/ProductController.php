<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

//Jobs
use App\Jobs\ProcessProduct;

//Views
use Illuminate\Support\Facades\View;


class ProductController extends Controller
{   

    private $url = 'https://jsonkeeper.com/b/EB4F';
    private $jsonData;

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   

        if($request->has('start')){
            $this->loadJson($this->url)->save();
        }
       
        return view('products.index',  ['name' => 'James']);
    }


    protected function loadJson($url)
    {
        $response = Http::get($url);

        $this->jsonData =  $response->body();

        Log::info('JSON Data Downloaded', [$url]);

        return $this;
    }

    protected function save(){

        $jsonData = json_decode($this->jsonData);

        Log::info('Product Total Count: ' . count($jsonData));

        if(!empty($jsonData)){
            foreach(array_chunk($jsonData, 5) as $indx => $data){

                $indx++;
                ProcessProduct::dispatch($data)->delay(now()->addSeconds((3 * $indx)));

                Log::info('Created and added for Queue as Job', [$data]);
            }
            
        }else{
            Log::critical('The source file is empty.', [$url]);
        }

        return json_decode($this->jsonData);
    }


}
