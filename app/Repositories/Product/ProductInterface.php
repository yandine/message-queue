<?php
	namespace App\Repositories\Product;


	interface ProductInterface{

		public function create($data);

		public function getAll();

		public function getLatest();

		public function find($id);
	}