<?php

namespace App\Repositories\Product;


use App\Repositories\Product\ProductInterface as ProductInterface;
use App\Models\Product;


class ProductRepository implements ProductInterface
{
    protected $product;


    function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function create($data)
    {   
       return $this->product->firstOrCreate([
                'id' => $data['id']
            ], 
            $data
        );
    }

    public function find($id)
    {   
       return $this->product->find($id)->first();
    }

    public function getAll()
    {
        return $this->product->all();
    }

    public function getLatest(){
        return $this->product->latest()->first();
    }
}